class Solution:
    def isPalindrome(self, x):
        """
        :type x: int
        :rtype: bool
        """
        if x < 0:
            return False
        elif x < 10:
            return True
        reverse = 0
        origin = x
        while x > 0:
            reverse = reverse*10 + (x%10)
            x //= 10
        if origin != reverse:
            return False
        return True