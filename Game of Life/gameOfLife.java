class Solution {
    public void gameOfLife(int[][] board) {
        for(int i = 0; i < board.length; i++) 
            for(int j = 0; j < board[0].length; j++) {
                int n = getLiveNeighborCount(board, i, j);
                if((board[i][j] == 1 && n == 2) || n == 3) board[i][j] += 2;
            }
        for(int i = 0; i < board.length; i++) 
            for(int j = 0; j < board[0].length; j++) 
                board[i][j] /= 2;
    }
    
    private int getLiveNeighborCount(int[][] board, int i, int j) {
        int xLeft = Math.max(0, i-1);
        int xRight = Math.min(board.length-1, i+1);
        int yTop = Math.max(0, j-1);
        int yBottom = Math.min(board[0].length-1, j+1);
        int count = 0;
        for(int x = xLeft; x <= xRight; x++)
            for(int y = yTop; y <= yBottom; y++)
                if(board[x][y] % 2 == 1)
                    count++;
        count -= board[i][j];
        return count;
    }
}