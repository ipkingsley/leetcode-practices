class Solution {
    public int singleNonDuplicate(int[] nums) {
        int l = 0;
        int r = nums.length-1;
        while(l < r) {
            int mid = (l+r)/2;
            if(nums[mid-1] == nums[mid])
                if((r-mid)%2 == 0)
                    r = mid;
                else
                    l = mid + 1;
            else if(nums[mid+1] == nums[mid])
                if((mid-l)%2 == 0)
                    l = mid + 2;
                else
                    r = mid - 1;
            else
                return nums[mid];
        }
        return nums[l];
    }
}