/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    private class TNode {
        TreeNode node;
        int row;
        int l;
        int r;
        
        TNode(TreeNode node, int row, int l, int r) {
            this.node = node;
            this.row = row;
            this.l = l;
            this.r = r;
        }
    }
    
    private int findDepth(TreeNode root) {
        if(root == null) return 0;
        return Math.max(findDepth(root.left), findDepth(root.right)) + 1;
    }
    
    public List<List<String>> printTree(TreeNode root) {
        if(root == null) return new ArrayList<List<String>>();
        int n = findDepth(root);
        int m = (int)Math.pow(2, n) - 1;
        List<List<String>> tree = new ArrayList<>();
        for(int i = 0; i < n; i++) {
            ArrayList<String> row = new ArrayList<>();
            for(int j = 0; j < m; j++)
                row.add("");
            tree.add(row);
        }
        Queue<TNode> q = new LinkedList<>();
        q.add(new TNode(root, 0, 0, m-1));
        while(!q.isEmpty()) {
            TNode cur = q.poll();
            int col = (cur.l + cur.r)/2;
            tree.get(cur.row).set(col, Integer.toString(cur.node.val));
            if(cur.node.left != null) {
                q.add(new TNode(cur.node.left, cur.row+1, cur.l, col-1));
            }
            if(cur.node.right != null) {
                q.add(new TNode(cur.node.right, cur.row+1, col+1, cur.r));
            }
        }
        
        return tree;
    }
}