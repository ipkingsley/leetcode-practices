class Solution {
    private String anagramsHashKeyHelper(String str) {
        if (str.isEmpty()) return "";
        char[] arr = str.toCharArray();
        Arrays.sort(arr);
        return String.valueOf(arr);
    }

    public List<List<String>> groupAnagrams(String[] strs) {
        if (strs.length == 1) return Collections.singletonList(Collections.singletonList(strs[0]));
        HashMap<String, List<String>> idxMap = new HashMap<>();
        for (String str : strs) {
            String hashCode = anagramsHashKeyHelper(str);
            idxMap.putIfAbsent(hashCode, new LinkedList<>());
            idxMap.get(hashCode).add(str);
        }
        return new ArrayList<>(idxMap.values());
    }
}
