class Solution {
    public List<List<Integer>> fourSum(int[] nums, int target) {
        List<List<Integer>> solution = new LinkedList<>();
        int length = nums.length;
        if(length < 4) return solution;
        Arrays.sort(nums);
        int pi = nums[0]-1;
        int length2 = length-2, length3 = length2-1;
        for(int i = 0; i < length3; i++) {
            if(nums[i] != pi) {
                int sum = nums[i] + nums[i+1] + nums[i+2] + nums[i+3];
                if(sum >= target) {
                    if(sum == target)
                        solution.add(Arrays.asList(nums[i],nums[i+1],nums[i+2],nums[i+3]));
                    break;
                }    
                int pj = pi;
                pi = nums[i];
                int diff0 = target - nums[i];
                for(int j = i+1; j < length2; j++) {
                    if(nums[j] != pj) {
                        pj = nums[j];
                        int k = j+1, l = length-1, diff = diff0 - pj;
                        while(k < l) {
                            int sum2 = nums[k] + nums[l];
                            if(sum2 > diff)
                                l--;
                            else if(sum2 == diff) {
                                solution.add(Arrays.asList(nums[i],nums[j],nums[k],nums[l]));
                                while(k < l && nums[k] == nums[k+1])
                                    k++;
                                while(k < l && nums[l] == nums[l-1])
                                    l--;
                                k++;
                                l--;
                            }
                            else
                                k++;
                        }
                    }
                }
            }
        }
        return solution;
    }
}