def solution1(nums):
    output = []
    length = len(nums)
    if length > 2:
        nums.sort()
        tu = tuple(nums)
        i = length - 1
        while i >= 2:
            target = -tu[i]
            j = 0
            k = i - 1
            while j < k:
                temp = tu[j] + tu[k]
                if temp < target:
                    j += 1
                elif temp > target:
                    k -= 1
                else:
                    output.append([tu[j], tu[k], tu[i]])
                    j += 1
                    k -= 1
                    while j < k and tu[j - 1] == tu[j]:
                        j += 1
                    while j < k and tu[k + 1] == tu[k]:
                        k -= 1
            i -= 1
            while i >= 2 and tu[i + 1] == tu[i]:
                i -= 1
    return output


def solution2(nums):
    output = []
    length = len(nums)
    if length > 2:
        nums.sort()
        tu = tuple(nums)
        i = 0
        oi = 1234567
        while i < length - 1:
            ni = tu[i]
            if ni == oi:
                i += 1
                continue
            oi = ni
            targets = {}
            j = i + 1
            while j < length:
                nj = tu[j]
                if nj in targets:
                    temp = targets[nj]
                    temp.append(nj)
                    output.append(temp)
                    while j + 1 < length and tu[j + 1] == nj:
                        j += 1
                else:
                    targets[0 - ni - nj] = [ni, nj]
                j += 1
            i += 1
    return output


class Solution:
    def threeSum(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """
        return solution2(nums)