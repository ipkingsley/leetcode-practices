class Solution {
    public int leastInterval(char[] tasks, int n) {
        int[] taskCounts = new int[26];
        int max = 0;
        int maxCount = 0;
        for(char task: tasks) {
            int count = ++taskCounts[task - 'A'];
            if(max < count) {
                max = count;
                maxCount = 0;
            }
            else if(max == count)
                maxCount++;
        }
        
        int idleCounts = Math.max(0, (max-1)*n + max + maxCount - tasks.length);
        return tasks.length + idleCounts;
    }
}