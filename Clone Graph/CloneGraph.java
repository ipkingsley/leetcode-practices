/*
// Definition for a Node.
class Node {
    public int val;
    public List<Node> neighbors;

    public Node() {}

    public Node(int _val,List<Node> _neighbors) {
        val = _val;
        neighbors = _neighbors;
    }
};
*/

class Solution {
    private Node[] array = new Node[100];
    
    public Node cloneGraph(Node node) {
        Node clone = new Node(node.val, new ArrayList<Node>());
        array[node.val-1] = clone;
        helper(node);
        return clone;
    }
    
    public void helper(Node node) {
        for (Node neighbor: node.neighbors) {
            if(array[neighbor.val-1] == null) {
                Node newNeighbor = new Node(neighbor.val, new ArrayList<Node>());
                array[neighbor.val-1] = newNeighbor;
                helper(neighbor);
            }
            array[node.val-1].neighbors.add(array[neighbor.val-1]);
        }
    }
}