class Solution {  
    public String reverseWords(String s) {
        if(s == null || s.length() == 0) return s;
        s += " ";
        char[] c = s.toCharArray();
        StringBuilder sb = new StringBuilder();
        int i = c.length - 1;
        int j = -1;
        for(; i >= 0; i--) {
            if(c[i] != ' ' && j == -1) {
                j = i + 1;
            }
            else if(c[i] == ' ' && j != -1) {
                sb.append(c, i+1, j-i);
                j = -1;
            }
        }
        if(c[0] != ' ')
            sb.append(c, 0, j);
        else if(sb.length() != 0)
            return sb.substring(0, sb.length()-1);
        return sb.toString();
    }
}