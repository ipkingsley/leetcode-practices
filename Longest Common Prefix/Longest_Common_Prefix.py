class Solution:
    def longestCommonPrefix(self, strs):
        """
        :type strs: List[str]
        :rtype: str
        """
        output = ""
        for x in zip(*strs):
            if len(set(x)) == 1:
                output += x[0]
            else:
                break
        return output