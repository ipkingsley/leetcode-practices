def solution1(digits):
    if not digits:
        return []
    letters = ((),(),('a','b','c'),('d','e','f'),('g','h','i'),('j','k','l'),('m','n','o'),('p','q','r','s'),('t','u','v'),('w','x','y','z'))
    if len(digits) == 1:
        return list(letters[int(digits)])
    digits = map(int, digits)
      
    result = [""]
    for digit in digits:
        result = [i + j  for i in result for j in letters[digit]]
    return result
    
def solution2(digits):
    if len(digits) == 0:
        return []
    dic = {}
    dic['2'] = ['a', 'b', 'c']
    dic['3'] = ['d', 'e', 'f']
    dic['4'] = ['g', 'h', 'i']
    dic['5'] = ['j', 'k', 'l']
    dic['6'] = ['m', 'n', 'o']
    dic['7'] = ['p', 'q', 'r', 's']
    dic['8'] = ['t', 'u', 'v']
    dic['9'] = ['w', 'x', 'y', 'z']
    if len(digits) == 1:
        return dic[digits]
    output = []
    for i in digits:
        if not output:
            output = dic[i]
        else:
            output = [j+k for j in output for k in dic[i]]
    return output

class Solution:
    def letterCombinations(self, digits):
        """
        :type digits: str
        :rtype: List[str]
        """
        return solution1(digits)
