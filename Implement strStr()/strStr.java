class Solution {
    public int strStr(String haystack, String needle) {
        int len2 = needle.length(); 
        if(len2 == 0)
            return 0;
        int len1 = haystack.length();
        if(len2 > len1)
            return -1;
        if(len1 <= 15) {
            len1 -= len2;
            char c0 = needle.charAt(0);
            for(int i = 0; i <= len1; i++) {
                if(haystack.charAt(i) != c0)
                    while (++i <= len1 && haystack.charAt(i) != c0);
                if(i <= len1) {
                    int j = 1;
                    for(; j < len2 && haystack.charAt(i + j) == needle.charAt(j); j++);
                    if(j == len2)
                        return i;
                }
            }
            return -1;
        }
        char [] chars1 = haystack.toCharArray();
        char [] chars2 = needle.toCharArray();
        char c0 = chars2[0];
        len1 -= len2;
        for(int i = 0; i <= len1; i++) {
            if(chars1[i] != c0)
                while (++i <= len1 && chars1[i] != c0);
            if(i <= len1) {
                int j = 1;
                for(; j < len2 && chars1[i + j] == chars2[j]; j++);
                if(j == len2)
                    return i;
            }
        }
        return -1;
    }
}