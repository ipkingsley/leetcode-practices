class Solution:
    def romanToInt(self, s):
        """
        :type s: str
        :rtype: int
        """
        total = 0
        pre = ' '
        for c in s:
            if c == 'I':            
                total += 1
            elif c == 'V':
                if pre == 'I':
                    total += 3
                else:
                    total += 5
            elif c == 'X':
                if pre == 'I':
                    total += 8
                else:
                    total += 10
            elif c == 'L':
                if pre == 'X':
                    total += 30
                else:
                    total += 50
            elif c == 'C':
                if pre == 'X':
                    total += 80
                else:
                    total += 100
            elif c == 'D':
                if pre == 'C':
                    total += 300
                else:
                    total += 500
            else:
                if pre == 'C':
                    total += 800
                else:
                    total += 1000
            pre = c
        return total