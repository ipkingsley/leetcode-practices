/* The isBadVersion API is defined in the parent class VersionControl.
      boolean isBadVersion(int version); */

public class Solution extends VersionControl {
    
    public int firstBadVersion(int n) {
        if(n == 1)
            return 1;
        int l = 1, r = n;
        int m = n / 2;
        while(l < r) {
            if(isBadVersion(m)) 
                r = m;
            else 
                l = m + 1;
            m = (int)(((long)l + r) / 2);
        }
        return m;
    }
}