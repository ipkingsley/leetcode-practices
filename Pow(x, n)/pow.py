class Solution:
    def powHelper(x, n):
        print(x, n, "\n")
        if n == 1:
            return x
        carry = int(n)%2
        base = int(n/2)
        if base <= 1:
            if carry == 0:
                return x*x
            else:
                return x*x*x
        if carry == 0:
            return Solution.powHelper(x = x*x, n = base)
        else:
            return x*Solution.powHelper(x = x*x, n = base)
    
    def myPow(self, x, n):
        """
        :type x: float
        :type n: int
        :rtype: float
        """
        if x == 0:
            return 0
        if x == 1.0:
            return x;
        if x == -1.0:
            if n%2 == 0:
                return -x;
            else:
                return x;
        if n == 0:
            return 1.0;
        if n > 0:
            return Solution.powHelper(x = x, n = n)
        else:
            return Solution.powHelper(x = 1/x, n = -n)