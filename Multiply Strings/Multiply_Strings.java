import java.util.*; 

class Solution {    
    
    public String multiply(String num1, String num2) {
        char[] n1 = num1.toCharArray();
        char[] n2 = num2.toCharArray();
        if (n1[0] == '0' || n2[0] == '0') return "0";
        int len1 = num1.length();
        int len2 = num2.length();
        if (len1 == 1 && num1.charAt(0) == '1') return num2;
        if (len2 == 1 && num2.charAt(0) == '1') return num1;
        int highestIdx = len1 + len2 - 1;
        for (int i = 0; i < len1; i++) n1[i] -= 48;
        for (int i = 0; i < len2; i++) n2[i] -= 48;

        char[] result = new char[highestIdx + 1];
        int idx1 = 0;
        for (int i = len1 - 1; i >= 0; i--) {
            int a = n1[i];
            if (a != 0) {
                int idx2 = 0, idx = highestIdx, carry = 0;
                for (int j = len2 - 1; j >= 0; j--) {
                    idx = highestIdx - idx1 - idx2;
                    int b = n2[j];
                    int tempSum = a * b + result[idx] + carry;

                    carry = tempSum / 10;
                    result[idx] = (char) (tempSum % 10);
                    idx2++;
                }

                if (carry > 0) {
                    result[--idx] += carry;
                }
            }
            idx1++;
        }

        int highIdx = 0;
        while (result[highIdx] == 0) {
            highIdx++;
        }
        for (int i = highIdx; i <= highestIdx; i++) {
            result[i] += 48;
        }

        return String.valueOf(result).substring(highIdx);
    }
}
