class Solution:
    def threeSumClosest(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: int
        """
        output = 1234567
        dif = 1234567
        length = len(nums)
        if length > 2:
            nums.sort()
            tu = tuple(nums)
            tu = nums
            i = length - 1
            while i >= 2:
                rhs = -tu[i] + target
                j = 0
                k = i - 1
                while j < k:
                    temp = tu[j] + tu[k]
                    if temp == rhs:
                        return target
                    else:
                        newDif = temp - rhs
                        if newDif > 0:
                            if dif > newDif:
                                dif = newDif
                                output = tu[i] + tu[j] + tu[k]
                            k -= 1
                            while j < k and tu[k + 1] == tu[k]:
                                k -= 1
                        else:
                            if dif > -newDif:
                                dif = -newDif
                                output = tu[i] + tu[j] + tu[k]
                            j += 1
                            while j < k and tu[j - 1] == tu[j]:
                                j += 1
                i -= 1
                while i >= 2 and tu[i + 1] == tu[i]:
                    i -= 1
        return output