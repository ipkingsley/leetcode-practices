class Solution {
    public List<Integer> findMinHeightTrees(int n, int[][] edges) {
        List<Integer> output = new LinkedList<>();
        if(n == 1) {
            output.add(0);
        }
        else if(n == 2) {
            output.add(0);
            output.add(1);
        }
        else {
            int[] degree = new int[n];
            ArrayList<Integer>[] adjList = new ArrayList[n];
            for(int i = 0; i < n; i++)
                adjList[i] = new ArrayList<>();
            for(int [] edge : edges) {
                degree[edge[0]]++;
                degree[edge[1]]++;
                adjList[edge[0]].add(edge[1]);
                adjList[edge[1]].add(edge[0]);
            }
            Queue<Integer> q = new LinkedList<>();
            int remain = n;
            boolean[] removed = new boolean[n];
            for(int i = 0; i < n; i++) {
                if(degree[i] == 1) {
                    q.add(i);
                    remain--;
                }
            }
            while(remain > 0) {
                int qSize = q.size();
                for(int i = 0; i < qSize; i++) {
                    int cur = q.poll();
                    for(int neighbor : adjList[cur]) {
                        if(--degree[neighbor] == 1) {
                            q.add(neighbor);
                            remain--;
                        }
                    }
                }       
            }
            output = (List)q;
        }
        
        return output;
    }
}