/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */

class Solution {
    private int helper(ListNode pre, ListNode cur, int count) {
        if(cur == null) 
            return count;
        count = helper(cur, cur.next, count) - 1;
        if(count == 0) {
            if(pre != null) 
                pre.next = cur.next;
            else {
                cur.val = cur.next.val;
                cur.next = cur.next.next;
            }
        }
        return count;
    }
    
    public ListNode removeNthFromEnd(ListNode head, int n) {
        if(head != null) {
            if(head.next != null)
                helper(null, head, n);
            else
                head = null;
        }
        return head;
    }
}