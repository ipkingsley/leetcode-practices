class Solution:
    def maxArea(self, height):
        """
        :type height: List[int]
        :rtype: int
        """
        tu = tuple(height)
        maxA = 0
        left = 0
        right = len(tu) - 1
        temp = 0
        while left < right:
            if tu[left] < tu[right]:
                temp = (right - left)*tu[left]
                if temp > maxA:
                    maxA = temp
                left += 1
            else:
                temp = (right - left)*tu[right]
                if temp > maxA:
                    maxA = temp
                right -= 1
        return maxA