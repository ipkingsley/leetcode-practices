class Solution {
    public boolean isValid(String s) {
        Map<Character,Character>map=new HashMap<>();
        map.put('(', ')');
        map.put('{', '}');
        map.put('[', ']');
        int size = 0;
        List<Character> list = new LinkedList<>();
        int length = s.length();
        if(length % 2 != 0) return false;
        for(int i = 0; i < length; i++){
            char c = s.charAt(i);
            if(c=='(' || c=='{' || c=='[') {
                list.add(map.get(c));
                size++;
            }
            else {
                if(size==0)
                    return false;
                if(list.get(size-1) == c){
                    list.remove(size-1);
                    size--;
                }
                else{
                    return false;
                }
            }
        }
        return size == 0;
    }
}