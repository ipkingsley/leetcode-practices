class Solution {
    
    public List<Integer> findSubstring(String s, String[] words) {
        List<Integer> result = new ArrayList<>();
        if(s.length() == 0 || words.length == 0) return result;
        int NumberOfWords = words.length;
        if(s.length() < (NumberOfWords*words[0].length())) return result;
        
        Map<String, Integer> counts = new HashMap<>();
        for (String word : words) {
            counts.put(word, counts.getOrDefault(word, 0) + 1);
        }
        
        int stringLength = s.length();
        int num = words.length; 
        int wordLen = words[0].length();
        
        for (int i = 0; i <= (stringLength - num * wordLen); i++) {
            Map<String, Integer> bucket = new HashMap<>();
            
            int j;
            for(j = 0; j < num; j++) {
                String word = s.substring(i + j*wordLen, i + (j+1)*wordLen);
                
                if (counts.containsKey(word)) {
                    bucket.put(word, bucket.getOrDefault(word, 0) + 1);
                    if (bucket.get(word) > counts.getOrDefault(word, 0)) {
                        break;
                    }
                } 
                else {
                    break;
                }
            }
            if (j == num) {
                result.add(i);
            }
        }
        return result;
    }  
    
}