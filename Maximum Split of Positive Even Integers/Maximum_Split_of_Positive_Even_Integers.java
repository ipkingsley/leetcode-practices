class Solution {
    private void arrayAddEvenRangeHelper(long n, Long[] resultList) {
        long j = 2;
        for (int i = 0; i < n; i++, j += 2) {
            resultList[i] = j;
        }
    }

    public List<Long> maximumEvenSplit(long finalSum) {
        if (finalSum % 2 == 1) return new ArrayList<>(0);
        Long[] result;

        long k = (long) (2 * Math.floor(Math.sqrt(finalSum)));
        long c = finalSum - ((k * (k + 2)) / 4);
        if (c == 0) {
            int capacity = (int) (k / 2);
            result = new Long[capacity];
            arrayAddEvenRangeHelper(capacity, result);
        } else if (c > 0) {
            int capacity = (int) (k / 2);
            result = new Long[capacity];
            arrayAddEvenRangeHelper(--capacity, result);
            result[capacity] = k + c;
        } else {
            int capacity = (int) (k / 2) - 1;
            result = new Long[capacity];
            arrayAddEvenRangeHelper(--capacity, result);
            result[capacity] = 2 * k + c - 2;
        }

        return Arrays.asList(result);
    }
}
