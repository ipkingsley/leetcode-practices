def matchChar(c1, c2):
    return (c1 == c2 or c2 == '.')

class Solution:
    
    def isMatch(self, s, p):
        """
        :type s: str
        :type p: str
        :rtype: bool
        """
        len_s = len(s)
        len_p = len(p)
        if len_s != 0 and len_p == 0 or (len_p == 1 and len_s > len_p):
            return False
        dp = [[False]*(len_p + 1) for k in range(len_s + 1)]
        dp[0][0] = True
        for i in range(1, len_p):
            dp[0][i + 1] = (p[i] == '*' and dp[0][i - 1])
        for i in range(len_p):
            for j in range(len_s):
                if matchChar(s[j], p[i]):
                    dp[j + 1][i + 1] = dp[j][i]
                elif p[i] == '*':
                    dp[j + 1][i + 1] = dp[j + 1][i - 1] or (matchChar(s[j], p[i - 1]) and dp[j][i + 1])
        return dp[len_s][len_p]
            