# Assuming the Table is called Scores
SELECT Score, Rank
FROM
    (SELECT 
        Score, 
        @prev := @curr,
        @curr := score,
        @rank := IF(@prev = @curr, CAST(@rank AS UNSIGNED), CAST(@rank+1 AS UNSIGNED)) AS Rank
    FROM Scores, 
        (SELECT @curr := null, @prev := null, @rank := 0) sell
    ORDER BY Score DESC) AS New;
;