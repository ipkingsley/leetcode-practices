/**
 * Definition for a point.
 * class Point {
 *     int x;
 *     int y;
 *     Point() { x = 0; y = 0; }
 *     Point(int a, int b) { x = a; y = b; }
 * }
 */
import java.math.BigDecimal;
import java.math.MathContext;
class Solution {
    public int maxPoints(Point[] points) {
        if(points.length <= 2) return points.length;
        int max = 0;
        
        for(int i = 0; i < points.length; i++) {
            int tempMax = 0;
            int sameCount = 0;
            HashMap<BigDecimal, Integer> slopeMap = new HashMap<>();
            for(int j = i+1; j < points.length; j++) {
                int dx = points[i].x - points[j].x;
                int dy = points[i].y - points[j].y;
                if(dx == 0 && dy == 0)
                    sameCount++;
                else {
                    int count = 2;
                    int slope = 0;
                    BigDecimal b = new BigDecimal(dy);
                    if(dx == 0)
                        b = new BigDecimal(Integer.MAX_VALUE);
                    else if(dy != 0) {
                        b = b.divide(BigDecimal.valueOf(dx), new MathContext(16));
                    }
                    if(slopeMap.containsKey(b)) {
                        count = slopeMap.get(b) + 1;
                        slopeMap.put(b, count);
                    }
                    else
                        slopeMap.put(b, 2);
                    if(count > tempMax)
                        tempMax = count;
                }
            }
            if(tempMax == 0)
                tempMax = sameCount+1;
            else
                tempMax += sameCount;
            if(tempMax > max)
                max = tempMax;
        }
        return max;
    }
}