class Solution {
    public int maximumGap(int[] nums) {
        int n = nums.length;
        if (n == 1) return 0;
        int min = Integer.MAX_VALUE, max = 0;
        for (int num : nums) {
            max = Math.max(num, max);
            min = Math.min(num, min);
        }
        if (max == min) return 0;
        int avgGap = (max - min + n - 2) / (n - 1);
        int[] lowerBoundArr = new int[n];
        int[] upperBoundArr = new int[n];
        Arrays.fill(lowerBoundArr, Integer.MAX_VALUE);
        for (int num : nums) {
            int idx = (num - min) / avgGap;
            lowerBoundArr[idx] = Math.min(lowerBoundArr[idx], num);
            upperBoundArr[idx] = Math.max(upperBoundArr[idx], num);
        }
        int maxGap = 0, preNum = upperBoundArr[0];
        for (int i = 1; i < n; i++) {
            if (lowerBoundArr[i] == Integer.MAX_VALUE) continue;
            maxGap = Math.max(maxGap, lowerBoundArr[i] - preNum);
            preNum = upperBoundArr[i];
        }
        return maxGap;
    }
}
